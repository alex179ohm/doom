;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Alessandro Cresto Miseroglio"
      user-mail-address "alex179ohm@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light))
;;(setq doom-font (font-spec :family "DejaVu Sans Mono" :size 14))
(setq doom-font (font-spec :family "Fira Code" :size 14))
;;(setq doom-font (font-spec :family "Hack" :size 15))
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'modus-operandi)

(setq modus-operandi-theme-mode-line '3d
      modus-operandi-theme-bold-constructs t
      modus-operandi-theme-slanted-constructs t
      modus-operandi-theme-syntax nil
      modus-operandi-theme-prompts 'subtle
      modus-operandi-theme-completions 'moderate
      modus-operandi-theme-fringes 'intense
      modus-operandi-theme-diffs 'desaturated
      modus-operandi-theme-org-blocks 'rainbow)


;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
;;(setq display-line-numbers-type t)
(setq display-line-numbers-type 'relative)

;; modeline
(setq doom-modeline-enable-word-count t)
(setq doom-modeline-continuous-word-count-modes '(markdown-mode gfm-mode org-mode))
;(setq doom-modeline-mu4e t)
(setq doom-modeline-indent-info nil)
(setq doom-modeline-github t)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(push 'company-files company-backends)

(require 'tex-mode)
(setcar (cdr (cddaar tex-compile-commands)) " -shell-escape ")

(setq +latex-viewers '(evince))

(global-prettify-symbols-mode)

(add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e")

(setq treemacs-git-mode 'extended)

(after! rustic
  :mode "\\.rs$"
  (setq rustic-format-on-save t))

;;(with-eval-after-load 'rustic-mode
;;  (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))

(use-package! org-super-agenda
  :after org-agenda
  :init
  (setq org-super-agenda-groups
        '((:name "Today"
           :time-grid t
           :habit t
           :scheduled today)
          (:name "Due Today"
           :deadline today)
          (:name "Important"
           :priority "A")
          (:name "Overdue"
           :deadline past)))
  :config
  (org-super-agenda-mode))

(use-package! org-fancy-priorities
  :hook (org-mode . org-fancy-priorities-mode)
  :config
  (setq org-fancy-priorities-list
        '((?A . "HIGH")
          (?B . "MID")
          (?C . "LOW")
          (?D . "OPTIONAL")
          (?I . "IMPORTANT"))))

(after! org
  (setq org-agenda-files '("~/org/work.org" "~/org/marghe.org" "~/org/home.org" "~/org/personal.org"))
  (setq org-default-notes-file (concat org-directory "notes.org"))
  (add-to-list 'org-modules 'org-habit)
  (setq org-habit-show-habits-only-for-today nil)
  (setq org-agenda-show-future-repeats 'next)
  (setq org-todo-keywords '((sequence "TODO(t)" "PROJ(p)" "NEXT(n)" "STARTED(s!)" "WAIT(w@/!)" "|" "DONE(d!)" "CANCELLED(c@)")
                            (sequence "BUG(b@)" "KNOWNCAUSE(k@)" "|" "FIXED(f!)")))
  org-todo-keyword-faces '(("[-]" . +org-todo-active)
                           ("TODO" . (:foreground "yellow" :weight bold-italic))
                           ("[?]" . +org-todo-onhold)
                           ("WAIT" . +org-todo-onhold)
                           ("PROJ" . +org-todo-project))
  (setq org-agenda-custom-commands '(("h" "Home and related" agenda "" ((org-agenda-files '("~/org/home.org" "~/org/marghe.org"))))
                                     ("p" "Personal and work" agenda "" ((org-agenda-files '("~/org/personal.org" "~/org/work.org")))))))

(after! mu4e
  (setq mu4e-root-maildir (expand-file-name "~/Maildir"))
  (setq mu4e-enable-async-operations t)
  (setq mu4e-get-mail-command "mbsync -a")
  (setq mu4e-view-show-images t)
  (setq mu4e-view-show-addresses t))

(set-email-account! "alex179ohm@gmail.com"
                    '((mu4e-sent-folder . "/[Gmail].Sent Mail")
                      (mu4e-drafts-folder . "/[Gmail].Drafts")
                      (mu4e-trash-folder . "/[Gmail].Trash")
                      (mu4e-refile-folder . "/[Gmail].All Mail")
                      (smtpmail-smtp-user . "alex179ohm@gmail.com")
                      (user-mail-address . "alex179ohm@gmail.com")) t)


(after! magit
  (setq magit-clone-default-directory "~/Projects/"))

(after! vterm
  (setq vterm-kill-buffer-on-exit t))

(after! lsp-ui
  (setq lsp-ui-peek-enable t))

(after! lsp-rust
  (setq lsp-rust-analyzer-cargo-watch-enable t
   lsp-rust-analyzer-cargo-watch-command "clippy"
   lsp-rust-analyzer-server-display-inlay-hints t
   lsp-rust-analyzer-display-parameter-hints t
   lsp-rust-analyzer-display-chaining-hints t))

(after! circe
  (set-irc-server! "Chat Freenode"
                   '(:tls t
                     :host "chat.freenode.net"
                     :port 6697
                     :nick "alex179ohm"
                     :sasl-password (lambda (&rest _) (+pass-get-secret "irc/freenode.net")))))



;; rust dap configuration: https://github.com/emacs-lsp/dap-mode/issues/366
(require 'dap-mode)
(defun get-rust-binary ()
  "Prompt user for which binary to use and return the filename."
  (interactive)
  (let* ((bin-name (read-string "Enter the target to execute: "))
         (filename (concat (projectile-project-root) "target/debug/" bin-name))
         (does-not-exist (not (file-exists-p filename))))
    (when does-not-exist (message "No binary found for especified target %s!" bin-name))
    filename))
(defun get-rust-args ()
  "Prompt user for args to pass."
  (interactive)
  (let ((args-string (read-string "Enter the target arguments: ")))
     (split-string args-string)))
(defun dap-lldb-rust--populate-start-file-args (conf)
  "Populate CONF with the required arguments."
  (-> conf
      (dap--put-if-absent :dap-server-path '("/usr/bin/lldb-vscode" "1441"))
      (dap--put-if-absent :port 1441)
      (dap--put-if-absent :type "lldb")
      (dap--put-if-absent :cwd (car `(,(projectile-project-root))))
      (dap--put-if-absent :program (car `(,(get-rust-binary))))
      (dap--put-if-absent :name "Rust Debug")
      (dap--put-if-absent :program-to-start "cargo build")
      (dap--put-if-absent :args (car `(,(get-rust-args))))))

(add-hook 'rustic-mode-hook (lambda ()
                              (dap-register-debug-provider "lldb" 'dap-lldb-rust--populate-start-file-args)
                              (dap-register-debug-template "Rust Run Configuration"
                                                           `(:type "lldb"
                                                             :cwd ,(projectile-project-root)
                                                             :request "launch"))))
